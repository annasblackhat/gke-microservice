package main

import (
    "log"
    "net/http"
    "fmt"
)


func helloServer(w http.ResponseWriter, req *http.Request) {
    w.Write([]byte("hello"))
}

func testPage(w http.ResponseWriter, req *http.Request) {
   for i:=0; i<1000000;i++ {
		res := 0
		for j:=0; j<10000; j++ {
			res = i * j + i / 2
			fmt.Println("loop...", res)
		}
		res = res + i
		fmt.Println("final res ", res)
	}
	w.Write([]byte("test page done!"))
}

func main() {
    http.HandleFunc("/hello", helloServer)
    http.HandleFunc("/test/", testPage)
    err := http.ListenAndServe(":8080", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
