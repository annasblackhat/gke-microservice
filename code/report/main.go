package main

import (
    "log"
    "net/http"
)

type Product struct {
	Id int
	Name string
}

func helloServer(w http.ResponseWriter, req *http.Request) {
    w.Write([]byte("hello this is from report service"))
}

func welcomePage(w http.ResponseWriter, req *http.Request) {
    w.Write([]byte("WELCOME PAGE REPORT"))
}

func main() {
   http.HandleFunc("/", welcomePage)
    http.HandleFunc("/hello", helloServer)

    err := http.ListenAndServe(":8080", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
