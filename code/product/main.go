package main

import (
    "log"
    "net/http"
    "io/ioutil"
    "fmt"
    "encoding/json"
)

type Product struct {
	Id int
	Name string
}

func helloServer(w http.ResponseWriter, req *http.Request) {
    w.Write([]byte("hello from product service"))
}

func welcomePage(w http.ResponseWriter, req *http.Request) {
    w.Write([]byte("WELCOME PAGE OF PRODUCT"))
}

func getReport(w http.ResponseWriter, req *http.Request) {
	resp, err := http.Get("http://report/hello")
    result := ""	
    if err != nil {
	  fmt.Println("Error - ", err)
         result = "Error bro.."
	}else{
	  defer resp.Body.Close()
	  body, _ := ioutil.ReadAll(resp.Body)
         result = string(body)
	  fmt.Println("Return - ", string(body))
	}
	w.Write([]byte(result))
}

func getProduct(w http.ResponseWriter, req *http.Request) {
	products := make([]Product,0)
	products = append(products, Product{1, "Yamie Panda Asin"})
	products = append(products, Product{2, "Paha Geprek"})

	js, err := json.Marshal(products)
  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  w.Header().Set("Content-Type", "application/json")
    w.Write(js)
}

func main() {
    http.HandleFunc("/",welcomePage)
    http.HandleFunc("/hello", helloServer)
    http.HandleFunc("/product", getProduct)
    http.HandleFunc("/test-report", getReport)

    err := http.ListenAndServe(":8080", nil)
    if err != nil {
        log.Fatal("ListenAndServe: ", err)
    }
}
