## SETUP CLUSTER
**Create default zone for compute engine to Singapure**

`
gcloud config set compute/zone asia-southeast1-a
`

**Create Kubernetes Engine Cluster** <br/>
Create cluster with one node only, named it as uniq-cluster

`
gcloud container clusters create uniq-cluster --num-nodes 1
`

**Get authentication credentials for the cluster** <br/>
`
gcloud container clusters get-credentials uniq-cluster --zone asia-southeast1-a
`

## PREPARE DOCKER IMAGE
Export PROJECT_ID. This will use to make docker image

`
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
`

Go to folder code/hello <br/>
Then try to build docker image using existing dockerfile

`
docker build -t gcr.io/${PROJECT_ID}/uniq-hello:v1 .
`

**Push docker image to Google Container Registry**

`
gcloud docker -- push gcr.io/${PROJECT_ID}/uniq-hello:v1
`

## PLAYING WITH KUBERNETES (yaml)
**Let's start with basic Kubernetes Implementation** <br/>
We will create one deployment and one service. Go to folder *kubernetes/start*

```$xslt
kubectl create -f deployments/hello.yaml
kubectl create -f services/hello.yaml
```

Wait for some minutes, and try to get your external IP address from the services that you created <br/>
`
kubectl get services
`


update configmap

`
kubectl create configmap nginx-frontend-conf --dry-run -o yaml | kubectl apply -f -
`




